class Student
  attr_reader :first_name, :last_name, :courses

  def initialize(first_name, last_name)
    @first_name = first_name.capitalize
    @last_name = last_name.capitalize
    @courses = []
  end

  def name
    "#{@first_name} #{@last_name}"
  end

  def enroll(new_course)
    return if @courses.include?(new_course)
    if @courses.any? { |course| course.conflicts_with?(new_course) }
      raise "Course Conlicts!"
    end
    @courses << new_course
    new_course.students << self
  end

  def course_load
    hash = Hash.new(0)
    courses.each { |course| hash[course.department] += course.credits }
    hash
  end





end
